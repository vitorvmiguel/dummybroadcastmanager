package bts.mdsd.android.dummybroadcastmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MyLocalReceiver extends BroadcastReceiver {
    private static final String TAG = MyLocalReceiver.class.getSimpleName();

    public MyLocalReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(MyLocalReceiver.TAG,"CUSTOM-LOCAL-RECEIVER event caught!");
        Toast.makeText(context,"Toast Local Intent",Toast.LENGTH_SHORT).show();
    }
}
