package bts.mdsd.android.dummybroadcastmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SystemBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = SystemBroadcastReceiver.class.getSimpleName();

    public SystemBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            Log.w(SystemBroadcastReceiver.TAG, "Power connected!");
        } else if (intent.getAction().equals("android.intent.action.AIRPLANE_MODE")) {
            Log.w(SystemBroadcastReceiver.TAG, "Airplane mode switched!");
        }
    }
}
