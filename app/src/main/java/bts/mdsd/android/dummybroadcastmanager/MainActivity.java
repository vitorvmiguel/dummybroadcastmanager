package bts.mdsd.android.dummybroadcastmanager;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver myLocalReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btnTriggerBroadcastReceiver = (Button) findViewById(R.id.btnTrigBroadcast);
        btnTriggerBroadcastReceiver.setOnClickListener(this);

        final Button btnLocalBroadcastReceiver = (Button) findViewById(R.id.btnTrigLocalBroadcast);
        btnLocalBroadcastReceiver.setOnClickListener(this);

        this.myLocalReceiver = new MyLocalReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                this.myLocalReceiver,new IntentFilter("bts.mdsd.android.dummybroadcastmanager.CUSTOM_LOCAL_INTENT"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.myLocalReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTrigBroadcast:
                Log.i(MainActivity.TAG, "Button Trigger Broadcast clicked!");
                Intent intent = new Intent();
                intent.setAction(getString(R.string.action_custom_broadcast));
                sendBroadcast(intent);
                break;
            case R.id.btnTrigLocalBroadcast:
                Log.i(MainActivity.TAG,"Button Local Broadcast clicked!");
                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        new Intent().setAction("bts.mdsd.android.dummybroadcastmanager.CUSTOM_LOCAL_INTENT"));
                break;
            default:
                Log.i(MainActivity.TAG, "In-onClick() default");
        }
    }
}
